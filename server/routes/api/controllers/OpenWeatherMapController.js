import axios from 'axios'

class OpenWeatherMapController {
  // more methods in the future:
}

OpenWeatherMapController.getFiveDays = (/* req, res */) => {
  const cityId = 6362368 // id for Bilbao as seen here: http://bulk.openweathermap.org/sample/
  const ApiKey = 'f06649cce480b649cdb2dc0f55e5f7e8'

  return axios.get('http://api.openweathermap.org/data/2.5/forecast?id=' + cityId + '&APPID=' + ApiKey)
}

export default OpenWeatherMapController
