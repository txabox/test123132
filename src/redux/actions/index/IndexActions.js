class IndexActions {
  /** **********************  INDEX SETTINGS  *************************/
  setTableData = tableData => {
    const _tableData = JSON.parse(JSON.stringify(tableData))
    return {
      type: 'INDEX_SET_TABLE_DATA',
      payload: {
        tableData: _tableData
      }
    }
  };
}

export default new IndexActions()
