import {
  createStore,
  applyMiddleware,
  combineReducers
} from 'redux'
import thunk from 'redux-thunk'
// Logger with default options
// import logger from 'redux-logger'
import indexReducer from './reducers/index/indexReducer'

const reducers = combineReducers({
  index: indexReducer
})

export default createStore(
  reducers,
  applyMiddleware(thunk/*, logger */))
