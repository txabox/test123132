const defaultValues = {
  tableData: []
}

const indexReducer = (state = defaultValues, action) => {
  const newState = {...state}

  if (action.type === 'INDEX_SET_TABLE_DATA') {
    newState.tableData = action.payload.tableData
  }

  return newState
}

export default indexReducer
