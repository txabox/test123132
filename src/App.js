import React from 'react'
import ReactDOM from 'react-dom'
import Router from './Router'

if (process.env.NODE_ENV !== 'production') {
  console.warn('Looks like we are in development mode!')
}

ReactDOM.render(
  (
    <div className="container">
      <Router />
    </div>
  ),
  document.getElementById('wiproTestApp')
)
