import React, { Component } from 'react'
import { ScaleLoader } from 'halogenium'
import PropTypes from 'prop-types'

import styled from 'styled-components'

class LoadingSpinner extends Component {
  render() {
    return (
      <div className={this.props.className}>
        <div className="loading">
          <div className="loading-internal">
            <ScaleLoader color='#666'/>
          </div>
        </div>
      </div>
    )
  }
}

LoadingSpinner.propTypes = {
  className: PropTypes.string
}

export default styled(LoadingSpinner)`
  .loading {
    box-sizing: border-box;
    display: flex;
    flex: 0 1 auto;
    flex-direction: row;
    flex-wrap: wrap;

    .loading-internal {
      align-items: center;
      display: flex;
      flex: 0 1 auto;
      flex-basis: 100%;
      flex-direction: column;
      flex-grow: 1;
      flex-shrink: 0;
      height: 400px;
      justify-content: center;
      max-width: 100%;
    }
  }
`
