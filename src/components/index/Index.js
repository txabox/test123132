import React from 'react'
import { Glyphicon, Button } from 'react-bootstrap'
import axios from 'axios'
import store from 'Redux/store'
import actions from 'Actions/actionCreators'
import PropTypes from 'prop-types'

import styled from 'styled-components'

import LoadingSpinner from 'Components/common/loadingSpinner/LoadingSpinner'
import ForecastTable from './components/ForecastTable'

class Index extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isForecastReceived: false,
      title: ''
    }
  }

  componentDidMount() {
    // init the app retrieving the 5 days forecast from the API
    this.getFiveDaysForecast()
  }

  _setInialAjaxReceived = (isForecastReceived) => {
    this.setState({
      isForecastReceived
    })
  }

  getFiveDaysForecast = (e) => {
    if (e) e.preventDefault()

    this._setInialAjaxReceived(false)

    axios.get('api/forecast/five/days')
      .then((response) => {
        if (response.status !== 200) {
          console.error('Error retrieving 5 days forecast. response: ', response)
          alert('Error retrieving 5 days forecast. Look at the console for more info.')
          this._setInialAjaxReceived(true)
        }

        const city = response.data.city
        this.setState({
          title: `Forecast for ${city.name} (${city.country})`
        }, () => {
          const setTableData = (response) => {
            const dataList = response.data.list
            const tableData = []

            // set the tableData
            for (let i = 0; i < dataList.length; ++i) {
              const el = dataList[i]
              // const date = new Date(el.dt*1000);

              const dateArray = el.dt_txt.trim().split(' ')
              const day = dateArray[0]
              const hour = dateArray[1]

              // Check if the day existed:
              const result = tableData.filter((obj) => {
                return obj.day === day
              })[0]

              const hourObj = {
                hour,
                el
              }

              if (!result) {
                let dayObj = {
                  day,
                  hours: [hourObj]
                }
                // the day didn't exist, we push it to the array
                tableData.push(dayObj)
              } else {
                // the day existed, but we push a new hour for that day
                result.hours.push(hourObj)
              }
            }

            store.dispatch(actions.index.setTableData(tableData))
          }
          setTableData(response)
          this._setInialAjaxReceived(true)
        })
      })
      .catch((error) => {
        console.error('Error retrieving 5 days forecast: ', error.response.data.message)
        alert('Error retrieving 5 days forecast. Look at the console for more info.')
        this._setInialAjaxReceived(true)
      })
  }

  render() {
    return (
      <div className={this.props.className}>

        {
          this.state.isForecastReceived
            ? <div className="body-container">

              <h1>{this.state.title}</h1>

              <ForecastTable />

              <div className="download-rules-btn">
                <Button bsStyle="success" onClick={this.getFiveDaysForecast}>
                  <Glyphicon glyph="refresh" />&nbsp;&nbsp;Refresh forecast
                </Button>
              </div>

            </div>
            : <LoadingSpinner />
        }

      </div>
    )
  }
}

Index.propTypes = {
  className: PropTypes.string
}

export default styled(Index)`
  h1 {
    margin-bottom: 30px;
  }

  .body-container {
    margin-bottom: 30px;
  }

  .download-rules-btn {
    margin-top: 20px;
    text-align: center;
  }
`
