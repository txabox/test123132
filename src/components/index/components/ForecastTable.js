import React from 'react'
import { Table } from 'react-bootstrap'
import store from 'Redux/store'
import PropTypes from 'prop-types'

import styled from 'styled-components'

import HourInfo from './HourInfo'

class ForecastTable extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      tableData: store.getState().index.tableData
    }

    this._HOUR_INTERVALS = [
      '00:00:00',
      '03:00:00',
      '06:00:00',
      '09:00:00',
      '12:00:00',
      '15:00:00',
      '18:00:00',
      '21:00:00'
    ]
  }

  componentDidMount() {
    this.unsubscribe = store.subscribe(() => {
      this.setState({
        tableData: store.getState().index.tableData
      })
    })
  }

  componentWillUnmount() {
    // clean timers, listeners...
    this.unsubscribe()
  }

  render() {
    return <Table className={this.props.className} responsive striped bordered condensed>
      <thead>
        <tr>
          <th>#</th>
          {
            this.state.tableData.map(dayObj => <th key={dayObj.day}>{dayObj.day}</th>)
          }
        </tr>
      </thead>
      <tbody>

        {
          this._HOUR_INTERVALS.map((hourInterval) => {
            return (
              <tr key={hourInterval}>
                <th>{hourInterval} h.</th>
                {
                  this.state.tableData.map((dayObj) => {
                    return (
                      <HourInfo
                        key={'hi_' + dayObj.day + '_' + hourInterval}
                        curHour={hourInterval}
                        dayHours={dayObj.hours} />
                    )
                  })
                }
              </tr>
            )
          })
        }

      </tbody>
    </Table>
  }
}

ForecastTable.propTypes = {
  className: PropTypes.string
}

export default styled(ForecastTable)`
  &.table {
    tbody > tr > td,
    tbody > tr > th,
    thead > tr > th {
      text-align: center;
      white-space: normal;
      word-break: break-all;
    }
  }
  @media (max-width: 768px) {
    &.table {
      tbody > tr > td,
      tbody > tr > th,
      thead > tr > th {
        font-size: .7em;
      }
    }

    h4 {
      font-size: 1.2em;
    }
  }
`
