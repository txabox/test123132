import React from 'react'
import PropTypes from 'prop-types'
import Utils from 'Utils/Utils'

import styled from 'styled-components'

class HourInfo extends React.Component {
  constructor(props) {
    super(props)

    const self = this

    let result = props.dayHours.filter(function(obj) {
      return obj.hour === props.curHour
    })[0]

    this.state = {
      el: null,
      hourMainForecast: null
    }

    if (result) {
      this.state.el = result.el

      this.state.hourMainForecast = Object.keys(this.state.el.main).map(key => {
        const title = key
          .replace('_', ' ') // replace underscore for space
          .replace(/\b\w/g, l => l.toUpperCase()) // Capitalize keys
        return <div key={key}>
          <strong>{title}:</strong> {self.state.el.main[key]}
        </div>
      })
    }
  }

  getClouds = () => {
    if (this.state.el && this.state.el.clouds && !Utils.isEmptyObject(this.state.el.clouds)) {
      return <div>
        <h4>Clouds:</h4>
        {this.state.el.clouds.all}%
      </div>
    } else {
      return null
    }
  }

  getWind = () => {
    if (this.state.el && this.state.el.wind && !Utils.isEmptyObject(this.state.el.wind)) {
      return <div>
        <h4>Wind:</h4>
        <div>
          <strong>Speed:</strong> {this.state.el.wind.speed} m/sec<br />
          <strong>Direction:</strong> {this.state.el.wind.deg}&deg;
        </div>
      </div>
    } else {
      return null
    }
  }

  getRain = () => {
    if (this.state.el && this.state.el.rain && !Utils.isEmptyObject(this.state.el.rain)) {
      return <div>
        <h4>Rain:</h4>
        {this.state.el.rain['3h']}mm
      </div>
    } else {
      return null
    }
  }

  getSnow = () => {
    if (this.state.el && this.state.el.snow && !Utils.isEmptyObject(this.state.el.snow)) {
      return <div>
        <h4>Snow:</h4>
        {this.state.el.snow['3h']}mm
      </div>
    } else {
      return null
    }
  }

  getMain = () => {
    return <div>
      { this.state.hourMainForecast ? <h4>Main:</h4> : null }
      { this.state.hourMainForecast }
    </div>
  }

  render() {
    return <td className={this.props.className}>

      { this.getClouds() }
      { this.getWind() }
      { this.getRain() }
      { this.getSnow() }
      { this.getMain() }

    </td>
  }
}

HourInfo.propTypes = {
  curHour: PropTypes.string.isRequired,
  dayHours: PropTypes.array.isRequired,
  className: PropTypes.string
}

export default styled(HourInfo)`
@media (max-width: 768px) {
  h4 {
    font-size: 1.2em;
  }
}
`
