const CopyWebpackPlugin = require('copy-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const path = require('path')

module.exports = {
  entry: path.resolve(__dirname, 'src/App.js'),
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: [{
          loader: 'style-loader' // creates style nodes from JS strings
        }, {
          loader: 'css-loader' // translates CSS into CommonJS
        }, {
          loader: 'sass-loader' // compiles Sass to CSS
        }]
      },
      {
        test: /\.json$/,
        use: 'json-loader'
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: 'babel-loader'
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin([
      path.join(__dirname, 'public')
    ], {
      verbose: true
    }),
    new CopyWebpackPlugin([
      {
        from: path.join(__dirname, '/src/views'),
        to: path.join(__dirname, '/public/views')
      },
      {
        from: path.join(__dirname, '/src/assets/images'),
        to: path.join(__dirname, '/public/assets/images')
      }
    ])
  ],
  resolve: {
    alias: {
      Components: path.resolve(__dirname, 'src/components/'),
      Redux: path.resolve(__dirname, 'src/redux/'),
      Actions: path.resolve(__dirname, 'src/redux/actions/'),
      Utils: path.resolve(__dirname, 'src/utils/')
    }
  }
}
